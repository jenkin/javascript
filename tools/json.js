/**
 * 判定Json是否为空
 */
function isEmptyJson(obj){
    for(var name in obj){
        return false;
    }
    return true;
}

/**
 * 判定元素类型是否Json
 */
function isJson(o){
    return typeof(o) == "object" && Object.prototype.toString.call(o).toLowerCase() == "[object object]" && !o.length;
}

/**
 * 判定元素类型是否数组
 */
function jsonToParam(json){
    var param = "", num = -1, arr = [];
    init(json);
    return param;

    function init(o){
        $.each(o, function(index, obj){
            if(isArray(obj)){
                num++;
                arr.push({name:index,type:"array"});
                init(obj);
            }else if(isJson(obj)){
                num++;
                arr.push({name:index,type:"json"});
                init(obj)
            }else{
                var p = "";
    			if(arr.length == 1 && arr[0].type == "array"){
					p = arr[0].name + "[" + index + "]=" + obj;
				}else{
					for(var i=0;i<arr.length;i++){
						if(arr[i-1] && arr[i-1].type == "array"){
							p = p.substr(0,p.length-1);
							p += "[" + arr[i].name + "].";
						}else{
							p += arr[i].name + ".";
						}
					}
					p += index + "=" + obj;
				}

				if(param){
					param += "&"
				}
				param += p;
            }
        });

        if(num >= 0){
            arr.splice(num);
            num--;
        }
    }

    function isArray(o) {
	    return Object.prototype.toString.call(o) === '[object Array]';
	}
	function isJson(o){
	    return typeof(o) == "object" && Object.prototype.toString.call(o).toLowerCase() == "[object object]" && !o.length;
	}
}