/**
 * 这个文件集成了常用的方法
 */

//Ajax请求集成方法
window.PDAjax = {
    _data: {},

    /**
     * 简易的Ajax请求，传递的参数可变化
     * @param url String 必选 请求的链接
     * @param data Object 可选
     * @param isReconnection Boolean 可选 默认为False
     * @param fn Function 必选 请求后的回调函数
     * @param type String 可选 默认为json
     * @constructor
     */
    simple: function (url,data,isReconnection,fn,type){
        var params = arguments;
        var json = {
            isAddQueue : true,
            url: params[0]
        };
        for(var i = 1; i<=params.length; i++){
            if(typeof params[i] == "function"){
                json.success = params[i];
                if(params[i+1]){
                    json.dataType = params[i+1];
                }
                if(i == 2){
                    if(typeof params[i-1] == "boolean"){
                        json.isReconnection = params[i-1];
                    }else{
                        json.data = params[i-1];
                    }
                }else if(i == 3){
                    json.isReconnection = params[i-1];
                    json.data = params[i-2];
                }
                break;
            }
        }
        this.main(json);
    },

    /**
     * 高度集合的Ajax请求
     * @param url String 请求的链接
     * @param data Object 请求的数据
     * @param type String 请求方式 默认为post，可选get
     * @param dataType String 请求数据格式 默认为json
     * @param success Function 请求成功后的回调函数
     * @param error Function 请求出错后的回调函数
     * @param loadText String 请求时的提示信息
     * @param failText String 请求出错后的提示信息
     * @param isAddQueue Boolean 把URL请求加入队列，同一时间不可重复请求 默认为false
     * @param isReconnection Boolean 请求失败后是否重连 默认为false
     * @param timeout Int 链接超时时间 默认为1000*60*3
     *
     */
    main: function(param){

        //默认值
        var defaults = {
            url : "",
            data : "",
            type : "get",
            dataType : "json",
            success : function(){},
            error : function(a, b, c){
                console.log(a);
                console.log(b);
                console.log(c);
            },
            loadText : "Loading",
            failText : "出错了",
            isAddQueue : false,
            isReconnection : false,
            timeout : 1000*60*3
        };
        var ops = $.extend({}, defaults, param);

        //如果url记录参数，则处理成Data数据
        if(!ops.data){
            var url_all = ops.url.split("?");
            ops.url = url_all[0];
            ops.data = (url_all.length > 1 ? url_all[1] : "");
        }

        var id = "loading_tip" + Math.round(Math.random()*100000000);

        //加入队列，记录各个请求的状态
        if(ops.isAddQueue){
            //check if url exist
            if(PDAjax._data[ops.url]){
                $("#"+PDAjax._data[ops.url]).find(".PDAjaxTip").html("请等待");
                return;
            }
            PDAjax._data[ops.url] = id;
        }

        _showLoading(id);

        $.ajax({
            url : ops.url,
            data : ops.data,
            type : ops.type,
            timeout : ops.timeout,
            dataType : ops.dataType,
            success : function(data){
                if(ops.isAddQueue){
                    $("#"+PDAjax._data[ops.url]).remove();
                    PDAjax._data[ops.url] = "";
                }else{
                    $("#"+id).remove();
                }
                ops.success(data);
            },
            error : function(a,b,c){
                if(ops.isAddQueue){
                    if(ops.isReconnection && b == "timeout"){
                        $("#"+PDAjax._data[ops.url]).find(".PDAjaxTip").html("请求超时，3秒后重新请求");
                        setTimeout(function(){
                            $("#"+PDAjax._data[ops.url]).remove();
                            PDAjax._data[ops.url] = "";
                            PDAjax.main(ops);
                        },3000);
                    }else{
                        if(b == "timeout"){
                            $("#"+PDAjax._data[ops.url]).find(".PDAjaxTip").html("请求超时");
                        }else{
                            $("#"+PDAjax._data[ops.url]).find(".PDAjaxTip").html(ops.failText);
                        }
                        setTimeout(function(){
                            $("#"+PDAjax._data[ops.url]).remove();
                            PDAjax._data[ops.url] = "";
                        },3000);
                    }
                }else{
                    $("#"+id).remove();
                }
                ops.error(a,b,c);
            }
        });

        //展示Loading提示
        function _showLoading(id){
            var ajaxDOM = $("#PDJSAJAX");
            if(ajaxDOM.length <= 0){
                ajaxDOM = $('<div id="PDJSAJAX" style="width: 200px;position: fixed;z-index:9999;left: 50%;top: 50%;margin: -15px 0 0 -100px;font-size: 32px;text-align:center;"></div>');
                ajaxDOM.appendTo("body");
            }

            var html = '<div id="'+ id +'" class="PDAjaxItem">';
            html += '<i class="fa fa-spinner fa-spin"></i><div class="PDAjaxTip" style="font-size: 13px;">'+ ops.loadText +'</div></div>';
            ajaxDOM.append(html);

        }
    }
};


window.PDMethod = {
    /**
     * 处理浏览器地址栏后面的参数，以json格式返回
     */
    processUrlParam: function(){
        var search = window.location.search, param = {}, arr;
        if(search){
            search = search.substring(1).split("&"); //去掉"?"，并把"&"切割
            for(var i=0; i<search.length; i++){
                arr = search[i].split("=");
                param[arr[0]] = arr[1];
            }
        }
        return param;
    }
};



/**
 * 检查Email的合法性
 */
function isValidEmail(isText){
	var reEmail = /^(?:\w+\.?)*\w+@(?:\w+\.?)*\w+$/;
	return reEmail.test(isText);
}