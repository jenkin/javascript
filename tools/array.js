/**
 * 判定元素类型是否数组
 */
function isArray(o) {
    return Object.prototype.toString.call(o) === '[object Array]';
}

/**
 * 判定元素是否在数组里
 */
Array.prototype.isInArray = function(param){
	var all = this;
	var signal = false;
	for(var i=0;i<all.length;i++){
		if(param === all[i]){
			signal = true;
			break;
		}
	}
	return signal;
};