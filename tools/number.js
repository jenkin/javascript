/**
 * 把数值变成百分比
 * 用法 var a = 2244.33;
 * 		a.toPercent(2)//变成有两位小数的百分数 224433%
 */
Number.prototype.toPercent = function(n){
	n = n || 0;
	var nu = ( Math.round( this * Math.pow( 10, n + 2 ) ) / Math.pow( 10, n ) ).toFixed( n );
	return nu + '%';
};

/**
 * 把数值变成N位小数
 * 用法 var a = 224.433;
 * 		a.toDecimail(2)//变成有两位小数 224.43
 */
Number.prototype.toDecimal = function(n){
	n = n || 0;
	return ( Math.round( this * Math.pow( 10, n)) / Math.pow( 10, n ));
};

/**
 * 两个数值相加，降低JS本身错误率
 */
function FloatAdd(arg1,arg2){
    var r1,r2,m;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    return (Math.round(arg1*m)+Math.round(arg2*m))/m;
}

/**
 * 判定是否数字
 */
function isNumber(value){
	//var r = /^[0-9]*[0-9][0-9]*$/;
	var r = /^\d+(\.\d+)?$/;
	return r.test(value);
}