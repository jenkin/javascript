/**
 * 首字母大写
 */
String.prototype.initialUppercase = function(){
	return this.replace(/(^|\s+)\w/g,function(s){return s.toUpperCase();})
};